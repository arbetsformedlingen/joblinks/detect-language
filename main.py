import argparse
import os
import sys
import time

import ndjson
from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException


def read_ads_stream_from_std_in():
    reader = ndjson.reader(sys.stdin)
    for ad in reader:
        yield ad


def read_ads_from_file(rel_filepath):
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    # Open input file with ads and load ndjson to dictionary...
    ads_path = currentdir + rel_filepath
    with open(ads_path, encoding="utf8") as ads_file:
        ads = ndjson.load(ads_file)
    return ads


def print_ad(ad):
    output_json = ndjson.dumps([ad], ensure_ascii=False)
    print(output_json, file=sys.stdout)

def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def str_to_bool(s):
    if s and (s.lower() == 'true' or s.lower() == 'yes'):
        return True
    else:
        return False

def print_debug(msg):
    if print_debug_messages:
        print(msg)

def detect_langcode(text_for_detection):
    if not text_for_detection:
        return "xx"

    # Note: If text to detect is in uppercase, the langdetect function might predict the language to be german instead of (correct) swedish.
    text_for_detection = text_for_detection.lower()
    try:
        lang_code = detect(text_for_detection)
        return lang_code
    except LangDetectException:
        # Can't detect lang_code
        # print('Error, text for detection: %s' % text)
        return "xy"



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Detects the language in the description for the ad')
    parser.add_argument('--filepath', help='Optional relative filepath to read ads from', required=False)

    parser.add_argument('--debug', help='Optional flag if debug information should be printed in std.out', required=False)

    args = parser.parse_args()

    global print_debug_messages
    print_debug_messages = str_to_bool(args.debug)

    filepath = args.filepath

    if filepath:
        ads = read_ads_from_file(filepath)
    else:
        ads = read_ads_stream_from_std_in()

    detected_lang_codes = []

    ts = time.time()

    for i, ad in enumerate(ads):
        try:
            description = ''
            text_for_detection = ''
            if 'originalJobPosting' in ad:
                # Note: If the title is included the language detection appears to work bad, since
                # some of the ads have different languages in the title compared to the description.
                if 'description' in ad['originalJobPosting'] and ad['originalJobPosting']['description']:
                    description = ad['originalJobPosting']['description']
                    if isinstance(description, list):
                        description = description[0]
                    text_for_detection += description

                text_for_detection = text_for_detection.strip()
                text_for_detection = text_for_detection[:400]

                detected_lang_code = detect_langcode(text_for_detection)

                detected_lang_codes.append(detected_lang_code)
                ad['detected_language'] = detected_lang_code

            print_ad(ad)

        except Exception as e:
            print_error(e)

    te = time.time()

    if print_debug_messages:
        from collections import Counter
        cnt = Counter()
        print_debug('Total time: %2.2f ms (%2.2f s)' % ((te - ts) * 1000, (te - ts)))
        print_debug('Total nr of ads: %s' % len(detected_lang_codes))
        print_debug('Detected languagecodes: %s' % Counter(detected_lang_codes).most_common(100))