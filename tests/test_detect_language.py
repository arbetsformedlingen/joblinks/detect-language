import os
import sys
import pytest
from main import detect_langcode


def test_detect_swedish():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'En mening utan html fast med radbrytning\nAndra raden kommer här'
    output = detect_langcode(input)
    assert 'sv' == output

def test_detect_english():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'Are you a senior hardware developer with at least +10 years of experience? Do you have experience in fiber optics? Are you ready for a great new opportunity?'
    output = detect_langcode(input)
    assert 'en' == output



def test_detect_norwegian():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'På vegne av en anerkjent og markedsledende bedrift innen sin bransje, er vi nå på utkikk etter flere fleksible budbilsjåfører! Vi ser etter deg som har førerkort klasse B og ønsker en jobb, i sentrale Oslo. Er det deg vi ser etter?'
    output = detect_langcode(input)
    assert 'no' == output


def test_detect_danish():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'Hvert år går velforsikrede danskere glip af flere millioner kr. i erstatning, som de er berettigede til gennem diverse ulykkesforsikringer. Hvorfor det? En af årsagerne er, at forsikringsprocesserne kan være meget komplekse og svære at gennemskue.'
    output = detect_langcode(input)
    assert 'da' == output


def test_detect_finnish():
    print('============================', sys._getframe().f_code.co_name, '============================ ')

    input = 'Nyt sinulla on mahdollisuus ainutlaatuiseen haasteeseen! Etsimme lastenhoitajaa suomenkieliseen Helsinki lastentarhaan.'
    output = detect_langcode(input)
    assert 'fi' == output



def test_detect_false_positive_german():
    print('============================', sys._getframe().f_code.co_name, '============================ ')
    # Note: If text to detect is in uppercase, the langdetect function predicts the language to be german (incorrect) instead of swedish (correct).
    input = 'Jobb Just Nu\nFÖRETAGET FORTSÄTTER ATT VÄXA, TROTS TUFFA TIDER, OCH DÄRFÖR SÖKER VI NU 1-2 PERSONER SOM KAN HJÄLPA OSS MED DET FORTSATTA UTVECKLINGSARBETET I KÖKET INKLUSIVE UTVECKLING AV YTTERLIGARE KONCEPT OCH VERKSAMHET.VI ERBJUDER EN UNIK MÖJLIGHET I JÖNKÖPING TILL PERSONLIG KOMPETENSUTVECKLING INOM NISCHEN FISK OCH SKALDJUR SAMT ATT VARA MED I UPPBYGGNADEN AV NYA SPÄNNANDE VERKSAMHETER.SOM KOCK I VÅRT KÖK FÅR MAN CHANSEN ATT ARBETA MED FÄRSKA RÅVAROR FRÅN HAV OCH SJÖ OCH VARA MED OM ATT UTVECKLA EN HÅLLBAR, TIDSENLIG OCH UPPSKATTAD PRODUKT TILLSAMMANS MED DUKTIGA MEDARBETARE. KOCKUTBILDNING ÄR ÖNSKVÄRT MEN ÄVEN RÄTT ERFARENHET INOM RESTAURANGKÖK OCH PERSONLIGA EGENSKAPER KAN KOMPENSERA FORMELL UTBILDNING. VI ERBJUDER ETT MODERNT OCH RYMLIGT KÖK OCH ETT HÄRLIGT TEAM AV ARBETSKAMRATER. BRA VILLKOR OCH FÖRMÅNER UTLOVAS FÖR RÄTT PERSON.'
    output = detect_langcode(input)
    assert 'sv' == output

def test_detect_empty_text():
    print('============================', sys._getframe().f_code.co_name, '============================ ')
    output = detect_langcode('')
    assert 'xx' == output

    output = detect_langcode(None)
    assert 'xx' == output

def test_detect_non_detectable():
    print('============================', sys._getframe().f_code.co_name, '============================ ')
    input = '...'
    output = detect_langcode(input)
    assert 'xy' == output


if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])